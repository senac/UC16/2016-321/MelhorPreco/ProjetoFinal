/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.wilson.silvino.melhorpreco.Bean;

import br.com.wilson.silvino.melhorpreco.DAO.BlogDAO;
import br.com.wilson.silvino.melhorpreco.Domain.Blog;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;
import org.omnifaces.util.Messages;

/**
 *
 * @author sala308b
 */
@ManagedBean
@ViewScoped
public class BlogBean implements Serializable{
    
    private Blog blog;
    private List<Blog> blogs;

    public Blog getBlog() {
        return blog;
    }

    public void setBlog(Blog blog) {
        this.blog = blog;
    }

    public List<Blog> getBlogs() {
        return blogs;
    }

    public void setBlogs(List<Blog> blogs) {
        this.blogs = blogs;
    }
    
    public void novo(){
        blog = new Blog();
    }
    
     public void salvar(){
         try {
             BlogDAO blogDAO = new BlogDAO();
             blogDAO.merge(blog);
             
             blog = new Blog();
             
             blogs = blogDAO.listar();
             
             Messages.addGlobalInfo("comentario salva com sucesso");
         } catch (RuntimeException erro) {
             Messages.addGlobalError("Ocorreu um erro ao tentar salvar o comentario ");
             erro.printStackTrace();
         }
     }
     
     @PostConstruct
     public void listar(){
         try {
             BlogDAO blogDAO = new BlogDAO();
             blogs = blogDAO.listar("Cadastro Blog");
         } catch (RuntimeException erro) {
             Messages.addGlobalError("Ocorreu um erro ao tentar listar o Blogs ");
             erro.printStackTrace();
         }
     }
     
     public void excluir(ActionEvent evento){
         try {
             
            blog = (Blog) evento.getComponent().getAttributes().get("BlogSelecionado");

            BlogDAO blogDAO = new BlogDAO();

            blogDAO.excluir(blog);

            blogs = blogDAO.listar();
            
           Messages.addGlobalInfo("Comentario removida com sucesso");  
         } catch (RuntimeException erro) {
              Messages.addFlashGlobalError("Ocorreu um erro ao tentar remover a Comentario ");
             erro.printStackTrace();
         }
     }
     
       public void editar(ActionEvent evento){
           blog = (Blog) evento.getComponent().getAttributes().get("BlogSelecionado");
       }
     
     
     
     
     
    
}
