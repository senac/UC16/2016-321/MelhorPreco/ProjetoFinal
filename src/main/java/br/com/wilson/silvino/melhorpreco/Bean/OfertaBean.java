/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.wilson.silvino.melhorpreco.Bean;

import br.com.wilson.silvino.melhorpreco.DAO.OfertaDAO;
import br.com.wilson.silvino.melhorpreco.Domain.Oferta;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;
import org.omnifaces.util.Messages;

/**
 *
 * @author Wilson
 */
@ManagedBean
@ViewScoped
public class OfertaBean implements Serializable {

    private Oferta oferta;
    private List<Oferta> ofertas;

    public Oferta getOferta() {
        return oferta;
    }

    public void setOferta(Oferta oferta) {
        this.oferta = oferta;
    }

    public List<Oferta> getOfertas() {
        return ofertas;
    }

    public void setOfertas(List<Oferta> ofertas) {
        this.ofertas = ofertas;
    }

    public void novo() {
        oferta = new Oferta();
    }

    public void salvar() {
        try {
            OfertaDAO ofertaDAO = new OfertaDAO();
            ofertaDAO.merge(oferta);

            oferta = new Oferta();

            ofertas = ofertaDAO.listar();

            Messages.addGlobalInfo("Oferta salva com sucesso");
        } catch (RuntimeException erro) {
            Messages.addGlobalError("Ocorreu um erro ao tentar salvar a Oferta ");
            erro.printStackTrace();
        }
    }

    @PostConstruct
    public void listar() {
        try {
            OfertaDAO ofertaDAO = new OfertaDAO();
            ofertas = ofertaDAO.listar("Oferta Cadastro");
        } catch (RuntimeException erro) {
            Messages.addGlobalError("Ocorreu um erro ao tentar listar as Ofertas ");
            erro.printStackTrace();
        }
    }

    public void excluir(ActionEvent evento) {
        try {
            oferta = (Oferta) evento.getComponent().getAttributes().get("OfertaSelecionado");

            OfertaDAO ofertaDAO = new OfertaDAO();
            ofertaDAO.excluir(oferta);

            ofertas = ofertaDAO.listar();
            Messages.addGlobalInfo("Oferta removido com sucesso");
        } catch (RuntimeException erro) {
            Messages.addFlashGlobalError("Ocorreu um erro ao tentar remover a Oferta ");
            erro.printStackTrace();
        }

    }

    public void editar(ActionEvent evento) {
        oferta = (Oferta) evento.getComponent().getAttributes().get("OfertaSelecionado");
    }

}
