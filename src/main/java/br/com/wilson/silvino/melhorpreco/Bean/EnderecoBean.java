/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.wilson.silvino.melhorpreco.Bean;

import br.com.wilson.silvino.melhorpreco.DAO.EnderecoDAO;
import br.com.wilson.silvino.melhorpreco.Domain.Endereco;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;
import org.omnifaces.util.Messages;

/**
 *
 * @author Wilson
 */
@ManagedBean
@ViewScoped
public class EnderecoBean implements Serializable {

    private Endereco endereco;
    private List<Endereco> enderecos;

    public Endereco getEndereco() {
        return endereco;
    }

    public void setEndereco(Endereco endereco) {
        this.endereco = endereco;
    }

    public List<Endereco> getEnderecos() {
        return enderecos;
    }

    public void setEnderecos(List<Endereco> enderecos) {
        this.enderecos = enderecos;
    }

    public void novo() {
        endereco = new Endereco();
    }

    public void salvar() {
        try {

            EnderecoDAO enderecoDAO = new EnderecoDAO();
            enderecoDAO.merge(endereco);

            endereco = new Endereco();

            enderecos = enderecoDAO.listar();

            Messages.addGlobalInfo("Endereço salvo com sucesso");
        } catch (RuntimeException erro) {
            Messages.addGlobalError("Ocorreu um erro ao tentar listar os Endereços ");
            erro.printStackTrace();
        }
    }

    @PostConstruct
    public void listar() {
        try {
            EnderecoDAO enderecoDAO = new EnderecoDAO();
            enderecos = enderecoDAO.listar("Cadastro Endereço");
        } catch (RuntimeException erro) {
            Messages.addGlobalError("Ocorreu um erro ao tentar listar os Endereços ");
            erro.printStackTrace();
        }
    }

    public void excluir(ActionEvent evento) {
        try {
            endereco = (Endereco) evento.getComponent().getAttributes().get("EnderecoSelecionado");

            EnderecoDAO enderecoDAO = new EnderecoDAO();
            enderecoDAO.excluir(endereco);

            enderecos = enderecoDAO.listar();
            Messages.addGlobalInfo("Endereço removido com sucesso");
        } catch (RuntimeException erro) {
            Messages.addFlashGlobalError("Ocorreu um erro ao tentar remover o Endereço");
            erro.printStackTrace();
        }
    }

    public void editar(ActionEvent evento) {
        endereco = (Endereco) evento.getComponent().getAttributes().get("EnderecoSelecionado");
    }

}
