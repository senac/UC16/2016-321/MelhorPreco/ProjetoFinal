/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.wilson.silvino.melhorpreco.Domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Diamond
 */
@Entity
public class Oferta extends GenericDomain implements Serializable{
    
    @Column(nullable = false)
 @Temporal(TemporalType.DATE)
    private Date data_inicio;
    
  @Column(nullable = true)
   @Temporal(TemporalType.DATE)
    private Date data_fim;
    
   @Column(nullable = false, precision = 12, scale = 2)
    private BigDecimal valor;

    public Date getData_inicio() {
        return data_inicio;
    }

    public void setData_inicio(Date data_inicio) {
        this.data_inicio = data_inicio;
    }

    public Date getData_fim() {
        return data_fim;
    }

    public void setData_fim(Date data_fim) {
        this.data_fim = data_fim;
    }

    public BigDecimal getValor() {
        return valor;
    }

    public void setValor(BigDecimal valor) {
        this.valor = valor;
    }
    
   
}
