/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.wilson.silvino.melhorpreco.Bean;

import br.com.wilson.silvino.melhorpreco.DAO.ClienteDAO;
import br.com.wilson.silvino.melhorpreco.Domain.Cliente;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;
import org.omnifaces.util.Messages;

/**
 *
 * @author Wilson
 */
@ManagedBean
@ViewScoped
public class ClienteBean implements Serializable {

    private Cliente cliente = new Cliente();
    private List<Cliente> clientes;

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public List<Cliente> getClientes() {
        return clientes;
    }

    public void setClientes(List<Cliente> clientes) {
        this.clientes = clientes;
    }

    public void novo() {
        cliente = new Cliente();
    }

    public void salvar() {
        try {

            ClienteDAO clienteDAO = new ClienteDAO();
            if (cliente.getCodigo() == null || cliente.getCodigo() == 0) {
                clienteDAO.salvar(cliente);
                clientes = clienteDAO.listar();

            } else {
                clienteDAO.merge(cliente);
            }

            Messages.addGlobalInfo("Cliente salvo com sucesso");

        } catch (RuntimeException erro) {
            Messages.addGlobalError("Ocorreu um erro ao tentar salvar o Cliente ");
            erro.printStackTrace();
        }
    }

    @PostConstruct
    public void listar() {
        try {
            ClienteDAO clienteDAO = new ClienteDAO();
            clientes = clienteDAO.listar("nome");

        } catch (RuntimeException erro) {
            Messages.addGlobalError("Ocorreu um erro ao tentar listar os Cliente ");
            erro.printStackTrace();
        }
    }

    public void excluir(ActionEvent evento) {
        try {
            cliente = (Cliente) evento.getComponent().getAttributes().get("clienteSelecionado");

            ClienteDAO clienteDAO = new ClienteDAO();
            clienteDAO.excluir(cliente);

            clientes = clienteDAO.listar();

            Messages.addGlobalInfo("Cliente removido com sucesso");
        } catch (RuntimeException erro) {
            Messages.addFlashGlobalError("Ocorreu um erro ao tentar remover o Cliente ");
            erro.printStackTrace();
        }

    }

  

}
